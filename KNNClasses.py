from TextVect import TextVect  # importation de la classe TextVect
import json

class KNNClasses:
  def __init__(self,description,data):
    self.description=description  # description de l'ensemble des classes
    self.data=data  # données pour chaque classe

  def add_class(self, label:str, vectors:list) :
    """
    Ajoute une nouvelle classe avec le label et les vecteurs fournis à l'ensemble de classes.

    Args:
    - label (str): label de la nouvelle classe
    - vectors (list): liste de vecteurs pour la nouvelle classe

    Raises:
    - ValueError: si le label de la classe existe déjà
    - TypeError: si le label ou les vecteurs ont un type invalide

    Returns:
    - None
    """
    # Vérification du type du label
    if not isinstance(label, str):
        raise TypeError("Le label doit être une chaîne de caractères")
    
    # Vérification du type des vecteurs
    if not isinstance(vectors, list):
        raise TypeError("Les vecteurs doivent être une liste")
    for vector in vectors:
        if not isinstance(vector, (list, tuple)):
            raise TypeError("Les vecteurs doivent être des listes ou des tuples")
    
    # Vérification que le label de la classe n'existe pas déjà
    for chaq_class in self.data:
        if label == chaq_class["label"]:
            raise ValueError("La classe '{}' existe déjà".format(label))
    
    # Ajout de la nouvelle classe
    new_class = {}
    new_class["label"] = label
    new_class["vectors"] = vectors
    self.data.append(new_class)

  def add_vector(self, label:str, vector) :
    """
    Ajoute un vecteur à une classe existante.

    Args:
    - label (str): label de la classe à laquelle ajouter le vecteur
    - vector (array-like): vecteur à ajouter à la classe

    Raises:
    - ValueError: si le label de la classe n'existe pas
    - TypeError: si le label ou le vecteur ont un type invalide

    Returns:
    - None
    """
    # Vérification du type du label
    if not isinstance(label, str):
        raise TypeError("Le label doit être une chaîne de caractères")
    
    # Vérification du type du vecteur
    if not isinstance(vector, (list, tuple)):
        raise TypeError("Le vecteur doit être une liste ou un tuple")
    
    # Recherche de la classe correspondant au label
    for (i, chaq_class) in enumerate(self.data):
        if label == chaq_class["label"]:
            self.data[i]["vectors"].append(vector)
            return
    
    # Si le label de la classe n'est pas trouvé
    raise ValueError("La classe '{}' n'existe pas".format(label))

  def del_class(self, label:str) :
    """
    Supprime une classe existante.

    Args:
    - label (str): label de la classe à supprimer

    Raises:
    - ValueError: si le label de la classe n'existe pas
    - TypeError: si le label a un type invalide

    Returns:
    - None
    """
    # Vérification du type du label
    if not isinstance(label, str):
        raise TypeError("Le label doit être une chaîne de caractères")
    
    # Recherche de la classe correspondant au label
    for chaq_class in self.data:
        if label == chaq_class["label"]:
            self.data.remove(chaq_class)
            return
    
    # Si le label de la classe n'est pas trouvé
    raise ValueError("La classe '{}' n'existe pas".format(label))

  def save_as_json(self, file_name:str):
    """
    Sauvegarde les données de l'ensemble de classes au format JSON dans un fichier.

    Args:
    - file_name (str): nom du fichier de sauvegarde

    Raises:
    - FileExistsError: si le fichier existe déjà
    - Exception: si une autre erreur se produit lors de la sauvegarde

    Returns:
    - bool: True si la sauvegarde a réussi, False sinon
    """
    data_jason= {'description': self.description, "data": self.data}
    try: 
      with open(file_name, encoding='utf-8', mode="x") as f:
        f.write(json.dumps(data_jason))
        print("Les données sont stockées dans un fichier au format json.")
      return True
    except FileExistsError:
      print("Erreur : le fichier '{}' existe déjà".format(file_name))
      return False
    except Exception as e:
      print("Erreur : {}".format(str(e)))
      return False

  def load_as_json(self, file_name:str):
    """
    Charge les données de l'ensemble de classes à partir d'un fichier JSON.

    Args:
    - file_name (str): nom du fichier à charger

    Raises:
    - FileNotFoundError: si le fichier n'existe pas
    - Exception: si une autre erreur se produit lors du chargement

    Returns:
    - bool: True si la charger a réussi, False sinon
    """

    try: 
      with open(file_name, encoding='utf-8', mode="r") as f:
        data_jason=json.load(f)
        self.description = data_jason['description']
        self.data = data_jason['data']
        print("Le contenu du fichier json a été chargé dans les données.")
      return True
    except FileNotFoundError:
      print("Erreur : le fichier '{}' n'existe pas".format(file_name))
      return False
    except Exception as e:
      print("Erreur : {}".format(str(e)))
      return False

  def classify(self, vector, k, sim_func) :
    """
    Cette fonction utilise l'algorithme knn pour classer le texte.
    """
    # Initialize empty lists and dictionaries for storing results
    results=[]
    results=[]   
    result={}   
    cls_sim={}  
    distance=[]  
    total_sim=[] 

    # Parcourt toutes les classes dans les données d'entraînement
    for chaq_class in self.data:
      # Parcourt tous les vecteurs de chaque classe
      for chaq_vector in chaq_class["vectors"]:
        # Calcule la similarité entre le vecteur d'entrée et le vecteur de la classe actuelle
        distance.append(sim_func(vector, chaq_vector))
        # Si le nombre de distances est supérieur ou égal à k, ne garde que les k plus grandes distances
        if len(distance)>=k:
          dis=sorted(distance, reverse=True)[:k]
        # Sinon, garde toutes les distances triées par ordre décroissant
        else:
          dis=sorted(distance, reverse=True)
      # Ajoute les k distances les plus grandes dans cls_sim pour chaque classe
      cls_sim[chaq_class["label"]]=dis
      # Réinitialise distance pour la prochaine classe
      distance=[]

    # Pour chaque classe, ajoute la similarité de chaque vecteur à total_sim
    for chaq_label in cls_sim.keys():
      result["label"]=chaq_label
      total_sim+=cls_sim[chaq_label]
    # Ne garde que les k plus grandes similarités totales
    total_sim=sorted(total_sim, reverse=True)[:k]

    # Pour chaque classe, calcule le nombre de vecteurs qui ont une similarité dans les k plus grandes
    # et calcule la moyenne des similarités pour ces vecteurs
    for relabel in cls_sim.keys():
      total=0
      n=0
      result = {}
      result["label"] = relabel
      # Parcourt toutes les similarités totales triées
      for i in total_sim:
        # Si la similarité fait partie des k similarités triées pour la classe actuelle,
        # incrémente le nombre de vecteurs ayant une similarité dans les k plus grandes et ajoute la similarité à total
        if i in cls_sim[relabel]:
          n+=1
          total+=i
      # Calcule la moyenne des similarités si n est différent de 0, sinon la moyenne est 0
      result["n"] = n
      if n == 0:
        result["average_sim"] = 0
      else:
        result["average_sim"] = total/n
      # Ajoute les résultats pour la classe actuelle à la liste de résultats
      results.append(result)
    # Retourne la liste de résultats
    return results
