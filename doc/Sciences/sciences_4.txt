Vous pouvez partager un article en cliquant sur les icônes de partage en haut à droite de celui-ci. 
La reproduction totale ou partielle d’un article, sans l’autorisation écrite et préalable du Monde, est strictement interdite. 
Pour plus d’informations, consultez nos conditions générales de vente. 
L’astronaute Jean-Jacques Favier, sixième Français à être allé dans l’espace, lors d’un vol à bord de la navette américaine Columbia, est mort à l’âge de 73 ans, a annoncé, vendredi 24 mars 2023, le Centre national d’études spatiales (CNES).
Né le 13 avril 1949 à Kehl, en Allemagne, Jean-Jacques Favier intègre l’Ecole nationale supérieure d’électrochimie et d’électrométallurgie de Grenoble en 1971. Il obtient un doctorat en ingénierie de l’Ecole des mines de Paris ainsi qu’un doctorat en physique et métallurgie de l’université Joseph-Fourier-Grenoble 1 en 1977.
Il est sélectionné en 1985 comme « astronaute expérimentateur » par l’agence spatiale française, alors qu’il est ingénieur de recherche au Commissariat à l’énergie atomique (CEA).
Au sein du CNES, il devient responsable scientifique du four spatial Mephisto, embarqué plusieurs fois à bord de la navette spatiale Columbia. En 1995, il est désigné comme astronaute spécialiste d’une expérience du laboratoire Spacelab, emporté par le vaisseau américain.
Lire aussi : Thomas Pesquet, dixième Français à partir dans l’espace
« Parcours exemplaire »
Il passe seize jours, vingt et une heures et quarante-huit minutes en orbite, du 20 juin au 7 juillet 1996. Soit quatorze ans après Jean-Loup Chrétien, le premier Français à avoir volé dans l’espace, à bord d’un vaisseau russe Soyouz.
Jean-Jacques Favier devient ainsi « le premier scientifique français à avoir séjourné dans l’espace », précise le CNES, qui rend hommage à son « parcours exemplaire ». « Il marquera de son empreinte les futures générations et inspirera nombre d’entre nous », ajoute le PDG du CNES, Philippe Baptiste, dans le communiqué.
Durant sa mission, Jean-Jacques Favier fut responsable de plus de trente expériences de physique en microgravité.
Après sa carrière d’astronaute, il s’était engagé dans l’éducation et la recherche, en collaborant notamment à un projet du CNES de préparation d’une future base lunaire ou martienne.