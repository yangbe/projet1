import re
import math 
import copy

class TextVect:
     # Expression régulière pour le tokenisation
    tok_grm=re.compile(r"""
        (?:etc.|p.ex.|cf.|M.)|
        \w+(?=(?:-(?:je|tu|ils?|elles?|nous|vous|leur|lui|les?|ce|t-|même|ci|là)))|
        [\w\-]+'?| # peut-être
        .
    """,re.X)

     # Méthode statique pour lire le fichier de liste d'arrêt et renvoyer un ensemble de mots à exclure
    def read_dict(stoplist_filename):
        dict_file = open(stoplist_filename, "r", encoding="utf-8")
        dict_content = dict_file.read()
        dict_file.close()
        stoplist = set(dict_content.split("\n"))
        return stoplist
    
     # Méthode pour filtrer les vecteurs en supprimant les mots dans la liste d'arrêt
    def filtrage(self, stop_words, vectors):
        doc_filtres = []
        for vector in vectors:
            if not isinstance(vector, dict):
                continue
            filtered_vector = {}
            for token in vector.keys():
                if token not in stop_words:
                    filtered_vector[token] = vector[token]
            doc_filtres.append(filtered_vector)
        return doc_filtres

    # Méthode pour diviser le texte en tokens à l'aide d'une expression régulière donnée
    def tokenize(self, text, tok_grm):
        return tok_grm.findall(text)

    # Méthode pour convertir la liste de tokens en un dictionnaire de fréquences de tokens
    def vectorise(self, tokens):
        token_freq={}
        for token in tokens:
            if token not in token_freq.keys():
                token_freq[token]=0 
            token_freq[token]+=1
        return token_freq
  
    # Méthode pour lire plusieurs fichiers de texte et renvoyer une liste de vecteurs
    def read_texts(self, file_names:list, tok_grm:str):
        vectors=[]
        for file_name in file_names :
            input_file=open(file_name,mode="r",encoding="utf8")
            tokens=[] 
            for line in input_file: 
                line=line.strip()
                toks=self.tokenize(line,tok_grm) 
                tokens.extend(toks)
            input_file.close()
            vector=self.vectorise(tokens)
            vectors.append(vector)
        return vectors
    
    # Méthode pour lire un fichier de texte et renvoyer un vecteur
    def read_text(self, file_name:str, tok_grm:str):
        vector=[]
        input_file=open(file_name,mode="r",encoding="utf8")
        tokens=[] 
        for line in input_file: 
            line=line.strip()
            toks=self.tokenize(line,tok_grm) 
            tokens.extend(toks)
        input_file.close()
        vector=self.vectorise(tokens)
        return vector
    
    # fonction tf_idf
    def tf_idf (self, documents:list)->list:
        documents_new=copy.deepcopy(documents)
        mots=set()

        for doc in documents:
            for word in doc["vect"]:
                mots.add(word)

        freq_doc={}
        for word in mots:
            for doc in documents:
                if word in doc["vect"]:
                    if word not in freq_doc:
                        freq_doc[word]=1
                    else :
                        freq_doc[word]+=1
  
        for doc in documents_new:
            for word in doc["vect"]:
                doc["vect"][word]=doc["vect"][word] / math.log(1+freq_doc[word])
        return documents_new

    #Cette fonction récupère deux vecteurs sous forme de hashage et renvoie leur produit scalaire
    def scalaire(self, vector1, vector2):
        liste_scalaire=[]
        for key in vector1:
            if key in vector2:
                liste_scalaire.append(vector1[key]*vector2[key])
        produit_scalaire=sum(liste_scalaire)
        return produit_scalaire

    #Cette fonction récupère un vecteur sous forme de hashage et renvoie sa norme
    def norme(self, vector):
        norme_carre=0
        for key in vector:
            norme_carre+=vector[key]*vector[key]
        norme=math.sqrt(norme_carre)
        return norme

    #Cette fonction récupère deux vecteurs sous forme de hashage, et renvoie leur cosinus
    def cosinus(self, vector1, vector2):
        norme1=self.norme(vector1)
        norme2=self.norme(vector2)
        scal=self.scalaire(vector1,vector2)
        cosinus=(scal/(norme1*norme2))
        return cosinus
