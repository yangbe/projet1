import os 
import re
from TextVect import TextVect 
from KNNClasses import KNNClasses
'''
Cette fonction peut utiliser l'algorithme knn pour classer le texte que vous fournissez et prend en charge les noms de balises et les vecteurs personnalisés.
Entrez vectoriser le texte, vous obtiendrez le vecteur du texte.
Entrez ajouter la class, vous ajouterez la classe aux données.
Entrez ajouter le vecteur, vous ajouterez un vecteur à cette classe.
Entrez supprimer la class, vous supprimerez cette classe.
Entrez enregistrer en json, vous enregistrez les données au format json.
Entrez charger en json, vous chargez des données au format json.
Entrez classer le texte, vous classerez le fichier.
Entrez quitter, vous quittez cette fonction.
'''

# Cette fonction permet de créer la liste des dossiers dans le répertoire "my_dir" et de filtrer les éléments qui ne sont pas des dossiers
def maindata():
    dossiers_name=[d for d in os.listdir(my_dir) if os.path.isdir(my_dir+"/"+d)]  

    # Création de la liste qui contiendra les données principales
    main_data=[]

    # Pour chaque dossier trouvé dans le répertoire, on va créer un dictionnaire qui contiendra les données
    for dossier_name in dossiers_name:
        chaq_class={}
        # On ajoute le nom du dossier comme étiquette dans le dictionnaire
        chaq_class["label"]=dossier_name
        # On ajoute le dossier en tant que liste
        dossier=[my_dir+"/"+dossier_name]
        # On crée une liste des fichiers dans le dossier, en filtrant les éléments qui ne sont pas des fichiers
        files_name=[f for f in os.listdir(my_dir+"/"+dossier_name) if not os.path.isdir(my_dir+"/"+dossier_name+"/"+f)]
        # On ajoute le chemin complet de chaque fichier à la liste "files"
        files=[my_dir+"/"+dossier_name+"/"+f for f in files_name]
        # On lit chaque texte dans les fichiers et on les vectorise
        texts_vectors=my_text_vect.read_texts(files, my_text_vect.tok_grm)
        # On filtre chaque document en utilisant la liste d'arrêt en français
        doc_filtres=my_text_vect.filtrage(french_stop_list, texts_vectors)
        # On ajoute les vecteurs de chaque document au dictionnaire
        chaq_class["vectors"]=doc_filtres
        # On ajoute le dictionnaire à la liste principale
        main_data.append(chaq_class)
    return main_data

#Cette fonction permet d'interagir avec l'utilisateur en utilisant l'algorithme knn pour classer du texte
def output():
    try:
        print("Cette fonction peut utiliser l'algorithme knn pour classer le texte que vous fournissez et prend en charge les noms de balises et les vecteurs personnalisés.\nEntrez vectoriser le texte, vous obtiendrez le vecteur du texte.\nEntrez ajouter la class, vous ajouterez la classe aux données.\nEntrez ajouter le vecteur, vous ajouterez un vecteur à cette classe.\nEntrez supprimer la class, vous supprimerez cette classe.\nEntrez enregistrer en json, vous enregistrez les données au format json.\nEntrez charger en json, vous chargez des données au format json.\nEntrez classer le texte, vous classerez le fichier.\nEntrez quitter, vous quittez cette fonction.")
        # Boucle while pour la saisie continue des choix de l'utilisateur
        while True:
            # Lecture du choix de l'utilisateur
            choix=input()
            
            # Si l'utilisateur choisit "vectoriser le texte", on lit le nom de fichier et on affiche le vecteur correspondant
            if choix == "vectoriser le texte":
                file_name=my_dir+"/"+input("Veuillez entrer le nom de fichier:")+".txt"
                print(my_text_vect.read_text(file_name,my_text_vect.tok_grm))
            
            # Si l'utilisateur choisit "ajouter la class", on lit le nom de la classe et ses vecteurs associés, et on ajoute la classe à la liste de données
            elif choix == "ajouter la class":
                label=input("Veuillez entrer le label:")
                vectors=list(input("Veuillez entrer les vectors:").split())
                Knndata.add_class(label,vectors)
            
            # Si l'utilisateur choisit "ajouter le vecteur", on lit le nom de la classe et le vecteur associé, et on l'ajoute à la liste de données
            elif choix == "ajouter le vecteur":
                label=input("Veuillez entrer le label:")
                vector=input("Veuillez entrer le vector:").split()
                Knndata.add_vector(label,vector)
            
            # Si l'utilisateur choisit "supprimer la class", on lit le nom de la classe et on la supprime de la liste de données
            elif choix == "supprimer la class":
                label=input("Veuillez entrer le label:")
                Knndata.del_class(label)
            
            # Si l'utilisateur choisit "enregistrer en json", on lit le nom de fichier et on enregistre les données dans ce fichier au format json
            elif choix == "enregistrer en json":
                file_name=my_dir+"/"+input("Veuillez entrer le nom de fichier:")+".json"
                Knndata.save_as_json(file_name)
            
            # Si l'utilisateur choisit "charger en json", on lit le nom de fichier et on charge les données depuis ce fichier au format json
            elif choix == "charger en json":
                file_name=my_dir+"/"+input("Veuillez entrer le nom de fichier:")+".json"
                Knndata.load_as_json(file_name)
            
            # Si l'utilisateur choisit "classer le texte", on lit le nom de fichier, la fonction de similarité et le paramètre k
            elif choix == "classer le texte":
                file_name=my_dir+"/"+input("Veuillez entrer le nom de fichier:")+".txt"
                if input("Veuillez entrer la function:")=="cosinus":
                    sim_func=my_text_vect.cosinus
                k=int(input("Veuillez entrer le k:"))
                vector=my_text_vect.read_text(file_name,my_text_vect.tok_grm)
                print(Knndata.classify(vector, k, sim_func))

            elif choix == "quitter":
                break

            else:
                print("Choix invalide. Veuillez choisir une option valide.")
    except FileNotFoundError:
        print("Le fichier n'a pas été trouvé. Veuillez vérifier le nom et le chemin d'accès.")
    except ValueError:
        print("Veuillez entrer un entier pour k.")
    except:
        print("Une erreur s'est produite.")

    
if __name__== "__main__" :
    # Définition du répertoire où se trouvent les fichiers à traiter
    my_dir = "./doc"    
    # Création d'une instance de la classe TextVect
    my_text_vect = TextVect() 
    # Définition du chemin d'accès au fichier de liste de mots à exclure
    stoplist_f = my_dir + "/stop_words_french.txt"
    # Lecture de la liste de mots à exclure depuis le fichier
    french_stop_list = TextVect.read_dict(stoplist_f)
    # Définition d'une chaîne de caractères pour la description
    description = "Nous avons quatre types de texte pour fournir des données classifiées:Planète, politique, sciences et sport."
    # Création d'une instance de la classe KNNClasses
    Knndata = KNNClasses(description, maindata())
    # Enregistrement des données dans un fichier JSON
    Knndata.save_as_json(my_dir + "/" + "data.json")
    output()
