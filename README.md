# Ce projet KnnClasses est de développer une série de classes Python qui utiliser l'algorithme de classification supervisé KNN pour classifier les textes.

## Description
Dans ce projet, on a trois fichiers python: "KNNClasses.py","TextVect.py","main.py" et aussi un dossier appelé "doc" avec beaucoup de textes. Dans ce dossier, on y stockons nos données textuelles classifiées, par exemple, je stocke les textes dans des dossiers tels que la science, la politique, le sport, etc. En même temps, je stocke le fichier "stop_words_french.txt" et le fichier que je souhaite classer dans le dossier "doc". Vous pouvez directement utiliser la fonction principale "main.py" pour personnaliser les données et classer le texte.

## API
### main.py
Cet API permet aux utilisateurs d'exécuter des commandes en entrant des exigences fixes. Cette fonction peut ajouter ou supprimer des classes, des vecteurs, sauvegarder ou charger des données au format JSON, utiliser l'algorithme knn pour classer le texte que vous fournissez et prend en charge les noms de balises et les vecteurs personnalisés.

#### maindata()
Cet fonction est utilisée pour traiter les données de texte classées stockées dans le dossier doc,Sa valeur de retour est la donnée principale.

#### output()
Cette fonction permet d'interagir avec l'utilisateur en utilisant l'algorithme knn pour classer du texte.

### KNNClasses.py
Cette classe KNN peut être utilisé pour classer les données représentées par des vecteurs. Il est utilisé pour classer les vecteurs d'entrée et renvoyer les étiquettes des K vecteurs les plus similaires. Ses méthodes incluent ajouter ou supprimer des classes, des vecteurs, sauvegarder ou charger des données au format JSON, utiliser l'algorithme knn pour classer le texte, le calcul de la similarité entre les vecteurs et le retour d'étiquettes pour les K vecteurs les plus proches.

#### init(self,description,data)
Paramètres:description (str), data (List)

#### add_class(self, label:str, vectors:list) 
Ajoute une nouvelle classe à l'objet KNNClasses.
Paramètres:label (str), vectors (list)

#### add_vector(self, label:str, vector)
Ajoute un vecteur à une classe existante.
Paramètres:label (str), vector (dict)

#### del_class(self, label:str)
Supprime une classe de l'objet KNNClasses.
Paramètres:label (str)

#### save_as_json(self, file_name:str)
Sauvegarde les données dans un fichier JSON.
Paramètres:filename (str)

#### load_as_json(self, file_name:str)
Charge les données à partir d'un fichier JSON.
Paramètres:filename (str)

#### classify(self, vector, k, sim_func) 
Classifie un vecteur en utilisant l'algorithme KNN.
Paramètres:vector (dict), k (int), sim_func (Callable)(Par défaut, la similarité cosinus)


### TextVect.py
Cette fonction peut utilisé pour vectoriser le texte. Il peut segmenter, vectoriser, lire des fichiers texte, filtrer des mots et calculer des valeurs TF-IDF et calculer la similarité cosinus.

#### read_dict(stoplist_filename)
Lecture d'une stoplist à partir d'un fichier
Input :arg1 : str - nom du fichier à lire. Un mot par ligne.
Output :valeur de retour : set(str) - ensemble des stopwords

#### filtrage(self, stop_words, vectors)
A partir d'une liste de documents (objets avec deux propriétés 'label' et 'vect')
on élimine tous les vocables appartenant à la stoplist.
Input :arg1 : set - l'ensemble des stopwords, arg2 : vectors - un vector est un dict

#### tokenize(self, text, tok_grm)
Input :arg1 : un texte à tokeniser, arg2 : une regex compilée pour la tokenisation
Output :valeur de retour : la liste des tokens

#### vectorise(self, tokens)
cette fonction récupère une liste de tokens, et renvoie un dictionnaire, contenant le vocabulaire avec les fréquences associées
Input :arg1 :tokens - list(str)
Output :valeur de retour : un dict contenant les vocables (clés) et les fréq associées (valeur)

#### read_texts(self, file_names:list, tok_grm:str)
Lit les textes à partir d'une liste de noms de dossiers.

#### read_text(self, file_name:str, tok_grm:str)
Lit un texte et retourne un dictionnaire contenant le label et les vecteurs du texte.

#### tf_idf (self, documents:list)
Calcul du TF.IDF pour une liste de documents
Input :arg1 : list(dict) : une liste de documents   
Output :valeur de retour : une liste de documents avec une modification des fréq associées à chaque mot (on divise par le log de la fréq de documents)

#### scalaire(self, vector1, vector2)
Cette fonction récupère deux vecteurs sous forme de hashage et renvoie leur produit scalaire
Input :arg1 : vector1 - hash,arg2 : vector2 - hash
Output :valeur de retour : un produit scalaire - float

#### norme(self, vector)
Cette fonction récupère un vecteur sous forme de hashage et renvoie sa norme
Input :arg1 : vector - hash
Output :valeur de retour : une norme - float

#### cosinus(self, vector1, vector2)
Cette fonction récupère deux vecteurs sous forme de hashage, et renvoie leur cosinus en appelant les fonctions Scalaire et Norme
Input :arg1 : vector1 - hash,arg2 : vector2 - hash
Output :valeur de retour : un cosinus - float

## Bogue
1.Dans la fonction classify de Knn, si la similitude de deux textes est la même, il peut y avoir des problèmes avec les données.
2.Dans les données traitées, certaines lettres françaises ne sont pas reconnues à cause de l'unicode.

## Amélioration
1.Pour le traitement des données principales : nous devons classer manuellement les données principales et stocker à l'avance les données classifiées dans le dossier sous le nom d'étiquette correspondant. Cette méthode est très gênante lorsqu'il s'agit d'une grande quantité de données.
2.Pour les données stockées au format json, nous devons charger et stocker manuellement. Si les utilisateurs souhaitent afficher les données au format json, ils doivent ouvrir manuellement les fichiers stockés dans le dossier doc.
3.Lors du calcul de la similarité, nous ne fournissons qu'une méthode de cosinus, en fait il existe d'autres méthodes de calcul de la similarité telles que la distance euclidienne etc.

## Utilisation
L'utilisateur peut ouvrir main.py pour s'exécuter, lors de l'exécution du terminal, vous devez entrer le dossier approprié, car ce programme utilise un chemin relatif. Puis entrer l'opération souhaitée en fonction de l'invite. Entrez vectoriser le texte, vous obtiendrez le vecteur du texte.Entrez ajouter la class, vous ajouterez la classe aux données. Entrez ajouter le vecteur, vous ajouterez un vecteur à cette classe. Entrez supprimer la class, vous supprimerez cette classe. Entrez enregistrer en json, vous enregistrez les données au format json. Entrez charger en json, vous chargez des données au format json. Entrez classer le texte, vous classerez le fichier. Entrez quitter, vous quittez cette fonction.

